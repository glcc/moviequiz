//
//  QuizManager.swift
//  MovieQuiz
//
//  Created by Gerson Costa on 05/12/2018.
//  Copyright © 2018 Gerson Costa. All rights reserved.
//

import Foundation

typealias Round = (quiz: Quiz, optins: [QuizOption])

class QuizManager {
    let quizes: [Quiz]
    let quizOption: [QuizOption]
    var round: Round?
    var score: Int
    
    init() {
        score = 0
        let quizesURL = Bundle.main.url(forResource: "quizes", withExtension: "json")!
        do {
            let quizesData = try Data(contentsOf: quizesURL)
            quizes = try JSONDecoder().decode([Quiz].self, from: quizesData)
            let quizOptionURL = Bundle.main.url(forResource: "options", withExtension: "json")!
            let quizOptionData = try! Data(contentsOf: quizOptionURL)
            quizOption = try! JSONDecoder().decode([QuizOption].self, from: quizOptionData)
        } catch {
            print(error.localizedDescription)
            quizes = []
            quizOption = []
        }
        
    }
    
    func generateRandomQuiz() -> Round {
        let quizIndex = Int(arc4random_uniform(UInt32(quizes.count)))
        let quiz = quizes[quizIndex]
        var indexes: Set<Int> = [quizIndex]
        while indexes.count < 4 {
            let index = Int(arc4random_uniform(UInt32(self.quizes.count)))
            indexes.insert(index)
        }
        let options = indexes.map({quizOption[$0]})
        round = (quiz, options)
        return round!
    }
    
    func checkAnswer(answer: String) {
        guard let round = round else {return}
        if answer == round.quiz.name {
            score += 1
        }
    }
    
}

struct Quiz: Codable {
    let name: String
    let number: Int
}

struct QuizOption: Codable {
    let name: String
}
