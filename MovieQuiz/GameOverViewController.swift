//
//  GameOverViewController.swift
//  MovieQuiz
//
//  Created by Gerson Costa on 05/12/2018.
//  Copyright © 2018 Gerson Costa. All rights reserved.
//

import UIKit

class GameOverViewController: UIViewController {

    @IBOutlet weak var lbScore: UILabel!
    var score = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lbScore.text = "\(score)"
    }

    @IBAction func playAgain(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
